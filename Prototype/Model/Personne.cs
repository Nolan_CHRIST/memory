﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Model
{
    class Personne
    {
        private string login;
        private string mdp;

        public Personne(string leLogin, string leMDP)
        {
            this.login = leLogin;
            this.mdp = leMDP;
        }

        public string getLogin()
        {
            return this.login;
        }

        public string getMDP()
        {
            return this.mdp;
        }

        public string description()
        {
            return "Login : "+this.login+"\nMDP :"+this.mdp;

        }
    }
}
