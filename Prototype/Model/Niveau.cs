﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Model
{
    class Niveau
    {
        private int numero = 0;
        private int nbreTotalCarte = 0;
        private int nbreColRang = 0;
        private int dimension = 0;

        public int getNumero()
        {
            return this.numero;
        }
        public int getNbreTotalCarte()
        {
            return this.nbreTotalCarte;
        }
        public int getNbreColRang()
        {
            return this.nbreColRang;
        }
        public int getDimension()
        {
            return this.dimension;
        }

        public Niveau(int numero, int nbreTotalCarte, int nbreColRang, int dimension)
        {
            this.numero = numero;
            this.nbreTotalCarte = nbreTotalCarte;
            this.nbreColRang = nbreColRang;
            this.dimension = dimension;
        }

        public string description()
        {
            return $"Le niveau à pour valeur\nNuméro {this.numero},\nNombre total de cartes {this.nbreTotalCarte}\nNombre de rangés ou de colonnes {this.nbreColRang}\nDimension de rangé ou de colone { this.dimension}";
        }
    }
}
