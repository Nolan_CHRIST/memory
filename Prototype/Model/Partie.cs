﻿using Prototype.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Model
{
    class Partie
    {
        private Niveau niveau;
        private Personne personne;
        private Carte[] lesCartes;
        private int nbreClics = 0;
        private string status = "En cours";


        public Partie(Niveau niveau, Personne personne, Carte[] lesCartesModeles)
        {
            int total;
            this.niveau = niveau;
            this.personne = personne;
            total = niveau.getNbreTotalCarte();
            this.lesCartes = new Carte[total];
            for(int i = 0; i < total/2; i++)
            {
                lesCartes[i] = lesCartesModeles[i];
                lesCartes[i + total / 2] = new Carte(lesCartes[i]);
            }
            lesCartes = LesServices<Carte>.MelangerLesCartes(lesCartes);

        }

        public int getNbreClics()
        {
            return this.nbreClics;
        }
        public void setNbreClics(int nbre)
        {
            this.nbreClics = nbre;
        }
        public string getStatus()
        {
            return this.status;
        }
        public void setStatus(string leStatus)
        {
            this.status = leStatus;
        }
        public Carte[] getLesCartes()
        {
            return this.lesCartes;
        }
        public Personne getPersonne()
        {
            return this.personne;
        }
        public Niveau getNiveau()
        {
            return this.niveau;
        }

        public string description() {
            return $"La partie comprend\n{this.niveau.description()}\net comprend\n{this.personne.description()}\navec les cartes suivantes";
        }
    }
}
