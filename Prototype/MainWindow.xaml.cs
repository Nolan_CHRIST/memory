﻿using Prototype.Controler;
using Prototype.Model;
using Prototype.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prototype
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            main.Content = new VuePersonne();
        }
        private void MenuItemFacile_Click(object sender, RoutedEventArgs e)
        {
            if (Jouer.getPersonne() != null)
            {
                Jouer.Initialiser(0);
                main.Content = new VueJouer();
            } else
            {
                MessageBox.Show("Connectez-vous avant de commencer une partie!");
            }
        }
        private void MenuItemMoyen_Click(object sender, RoutedEventArgs e)
        {
            if (Jouer.getPersonne() != null)
            {
                Jouer.Initialiser(1);
                main.Content = new VueJouer();
            }
            else
            {
                MessageBox.Show("Connectez-vous avant de commencer une partie!");
            }
        }
        private void MenuItemDifficile_Click(object sender, RoutedEventArgs e)
        {
            if (Jouer.getPersonne() != null)
            {
                Jouer.Initialiser(2);
                main.Content = new VueJouer();
            }
            else
            {
                MessageBox.Show("Connectez-vous avant de commencer une partie!");
            }
        }
    }
}
