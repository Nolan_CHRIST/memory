﻿using Prototype.Controler;
using Prototype.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Prototype.View
{
    /// <summary>
    /// Logique d'interaction pour VueJoueur.xaml
    /// </summary>
    public partial class VueJouer : Page
    {
        private bool rafraichir;

        public VueJouer()
        {
            InitializeComponent();
            for (int i = 0; i < Jouer.getPartie().getNiveau().getDimension(); i++)
            {
                RowDefinition row = new RowDefinition();
                ColumnDefinition column = new ColumnDefinition();

                row.Height = new GridLength(Jouer.getPartie().getNiveau().getDimension());
                column.Width = new GridLength(Jouer.getPartie().getNiveau().getDimension());

                GdFond.RowDefinitions.Add(row);

                GdFond.ColumnDefinitions.Add(column);
            }

            int h = Jouer.getPartie().getLesCartes().Length;

            for(int i = 0; i < h; i++)
            {
                GdFond.Children.Add(Jouer.getPartie().getLesCartes()[i].GetBouton());
            }

            int k = 0;
            
            for (int i = 0; i < Jouer.getPartie().getNiveau().getNbreColRang(); i++)
            {
                for (int j = 0; j < Jouer.getPartie().getNiveau().getNbreColRang(); j++)
                {
                    Grid.SetRow(GdFond.Children[k], i);
                    Grid.SetColumn(GdFond.Children[k], j);
                }
                k++;
            }

            for (int i = 0; i < Jouer.getPartie().getNiveau().getNbreTotalCarte(); i++)
            {
                Jouer.getPartie().getLesCartes()[i].GetBouton().Click += new RoutedEventHandler(BoutonClick);
            }
        }

        private void BoutonClick(object sender, RoutedEventArgs e)
        {
            foreach(Carte elem in Jouer.getPartie().getLesCartes())
            {
                if (elem.GetBouton() == sender)
                {
                    Jouer.JouerUnTour(elem);
                }
            }
        }
    }
}
