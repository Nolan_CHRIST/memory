﻿using Prototype.Controler;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prototype.View
{
    /// <summary>
    /// Logique d'interaction pour VuePersonne.xaml
    /// </summary>
    public partial class VuePersonne : Page
    {
        public VuePersonne()
        {
            InitializeComponent();
        }
        private void btn_valider_Click(object sender, RoutedEventArgs e)
        {
            if (txt_login.Text !="" && txt_login.Text != "")
            {
                Jouer.setPersonne(new Model.Personne(txt_login.Text, txt_mdp.Text));
                MessageBox.Show("Veuillez êtes bien connecté!");
            } else
            {
                MessageBox.Show("Veuillez renseigner le login et le mot de passe");
            }
        }
    }
}
