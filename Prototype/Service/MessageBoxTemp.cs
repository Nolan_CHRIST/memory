﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Drawing;

namespace Prototype.Service
{
    public class MessageBoxTemp
    {
        System.Threading.Timer _timeoutTimer;
        string _caption;
        MessageBoxTemp(string text, string caption, int timeout)
        {
            _caption = caption;
            _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
            null, timeout, System.Threading.Timeout.Infinite);
            FindAndMoveMsgBox(-300, -300, true, caption);
            MessageBox.Show(text, caption);
        }
        public static void Show(string text, string caption, int timeout)
        {
            new MessageBoxTemp(text, caption, timeout);
        }
        void OnTimerElapsed(object state)
        {
            IntPtr mbWnd = FindWindow(null, _caption);
            if (mbWnd != IntPtr.Zero)
                SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            _timeoutTimer.Dispose();
        }
        public static void FindAndMoveMsgBox(int x, int y, bool repaint,
        string title)
        {
            Thread thr = new Thread(() =>
            {
                IntPtr msgBox = IntPtr.Zero;
                while ((msgBox = FindWindow(IntPtr.Zero, title)) ==
                IntPtr.Zero) ;
                Rectangle r = new Rectangle();
                GetWindowRect(msgBox, out r);
                MoveWindow(msgBox, x, y,
                r.Width - r.X,
                r.Height - r.Y,
                repaint);
            });
            thr.Start();
        }
        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError =
        true)]
        static extern IntPtr FindWindow(string lpClassName, string
        lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet =
        System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr
        wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        static extern IntPtr FindWindow(IntPtr classname, string title);
        [DllImport("user32.dll")]
        static extern void MoveWindow(IntPtr hwnd, int X, int Y,
        int nWidth, int nHeight, bool rePaint);
        [DllImport("user32.dll")]
        static extern bool GetWindowRect(IntPtr hwnd, out Rectangle rect);
    }
}
